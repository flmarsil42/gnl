#ifndef GET_NEXT_LINE_BONUS_H
# define GET_NEXT_LINE_BONUS_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>

// # define BUFFER_SIZE 10000

int			get_next_line(int fd, char **line);
char		*ft_substr(char **rst, unsigned int start);
void		ft_memset(char *buf, size_t count);
char		*ft_strjoin(char **s1, char *s2);
char		*ft_strnew(size_t size);
char		*ft_strdup(char *rst);
int			check_bsn(char *buf);
size_t		ft_strlen(char *s);

#endif
