#include "get_next_line_bonus.h"

int		check_bsn(char *rst)
{
	int i;

	i = 0;
	if (!rst)
		return (0);
	while (rst[i] && rst[i] != '\n')
		i++;
	return ((rst[i] == '\n') ? 1 : 0);
}

size_t	ft_strlen(char *s)
{
	int	i;

	i = 0;
	if (!s)
		return (0);
	while (s[i])
		i++;
	return (i);
}

void	ft_memset(char *buf, size_t count)
{
	size_t i;

	i = -1;
	while (++i < count)
		((unsigned char *)buf)[i] = 0;
	return ;
}

char	*ft_strnew(size_t size)
{
	char *ret;

	if (!(ret = malloc(sizeof(char) * size + 1)))
		return (NULL);
	ft_memset(ret, size + 1);
	return (ret);
}

char	*ft_strjoin(char **rst, char *buf)
{
	char	*ret;
	size_t	size;
	size_t	i;
	size_t	j;

	size = (ft_strlen(*rst) + ft_strlen(buf) + 1);
	if (!(ret = malloc(sizeof(char) * size)))
		return (NULL);
	i = -1;
	j = 0;
	while (*rst && (*rst)[j])
		ret[++i] = (*rst)[j++];
	while (buf && *buf)
		ret[++i] = *buf++;
	ret[++i] = '\0';
	if (*rst)
		free(*rst);
	return (ret);
}
