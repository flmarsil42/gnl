# Get next line

## Résumé
Ce projet a pour but de nous faire coder une fonction qui renvoit une ligne terminée par un retour à la ligne lue depuis un descripteur de fichier.
Il nous permet donc d'aborder les notions de descripteurs de fichiers, de lecture et de l'utilisation des buffers.

Skills:
- Unix
- Rigor
- Algorithms & AI 

Lire le [sujet][1].

`Ce projet a été codé sur Macos`

### Compiler et lancer le projet

1. Téléchargez / Clonez le dépot

```
git clone https://gitlab.com/flmarsil42/get_next_line && cd get_next_line/
```

2. Copiez votre main.c dans le répertoire racine ou utilisez celui déjà présent, puis compilez les fichiers .c .

Vous devez indiquer la taille du buffer que get_next_line utilisera.

```
gcc -Wall -Werror -Wextra -D BUFFER_SIZE=10 main.c get_next_line.c get_next_line_utils.c
```

[1]:https://gitlab.com/flmarsil42/get_next_line/-/blob/master/fr.subject.pdf
